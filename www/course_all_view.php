
	<?php
	require '../php/connection.php';
    $sqlb = "SELECT id,course_name,course_description from courses ORDER BY id DESC"; 
    $resultb = mysqli_query($conn, $sqlb);
 $sql_title_courses = "SELECT * from home_text"; 
    $result_title_courses = mysqli_query($conn, $sql_title_courses);
	?>

<div style="background-color: #cbcbcb">
<div class="container"  >
	<div class="courses">
		<div class="courses-title">
			AVAILABLE COURSES
		</div>
		<div class="courses-sub">
			<?php
							while($row_title_courses = mysqli_fetch_array($result_title_courses)) {
						?>
						<?php  
							if($row_title_courses["title"]=="courses_sub"){
						?>
							<p  id="courses_sub" contenteditable="true">
									
									<?php echo $row_title_courses["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_courses_sub()" id="save_courses_sub">Save</button>
		</div>

		<div class="courses-content">
			<div class="row" > 

				<?php
						while($rowb = mysqli_fetch_array($resultb)) {
					?>
				<div class="col-md-4">
						<div class="card">
						  <img src="../imgs/course1.png">
				 			
						  <div class="card-footer">
						  		<div class="card-footer-title">
						  			<?php echo $rowb['course_name'];?>
						  		
						  		</div>
						  		<div class="card-footer-text">
						  				<?php echo $rowb['course_description'];?>
						  		</div>
						  		<div class="form-row text-center">
								    <div class="col-12">
								    	<button type="button" id="one-course" class="btn ">READ MORE</button>
								    </div>
								</div>
						  		
						  </div>
						</div>	<br>
				</div>
				
		
								<?php		
			}
		    
		?>
			 
			</div>

			
			
		</div>
	</div>
</div>
</div>



<script>
	document.getElementById('save_courses_sub').style.visibility="hidden";

	document.getElementById("courses_sub").addEventListener("input", function() {
   	document.getElementById('save_courses_sub').style.visibility="visible";
}, false);

	function save_courses_sub(){
		var a = document.getElementById('courses_sub').innerHTML;
		window.location.href = '../php/save_courses_sub_page.php?courses_sub=' + a ;
	}
</script>