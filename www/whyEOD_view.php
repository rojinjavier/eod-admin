<?php
  
    $sql_why = "SELECT * from home_text"; 
    $result_why = mysqli_query($conn, $sql_why);

?>

<div class="container-fluid" style="background-color: #cbcbcb">
	<div class="container">
		<div class="why">
			<div class="why-title">
				WHY EOD ?
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="why-text-left">
						<?php
							while($row_why = mysqli_fetch_array($result_why)) {
						?>
						<?php  
							if($row_why["title"]=="why_eod_right"){
						?>
							<p  id="why_eod_right" contenteditable="true">
									
									<?php echo $row_why["content"]; ?>
							</p><?php  ?>
				<button type="button" class="btn btn-link" onclick="save_why_eod_right()" id="save_why_eod_right">Save</button>
					</div>
				</div>
				<div class="col-md-6">
					<br>
					<div class="why-text-right" >
					 	<div class="icon"><i class="ion-checkmark"></i>
						<?php }
							if($row_why["title"]=="why_eod_left1"){
						?>
							<span  id="why_eod_left1" contenteditable="true">
									<?php echo $row_why["content"]; ?>
							</span>
									<?php  ?>
							<button type="button" class="btn btn-link" onclick="save_why_eod_left1()" id="save_why_eod_left1">Save</button>
					 	</div>
					 	<div class="icon"><i class="ion-checkmark"></i>
						<?php }
							if($row_why["title"]=="why_eod_left2"){
						?>
							<span  id="why_eod_left2" contenteditable="true">
									<?php echo $row_why["content"]; ?>
							</span>
									<?php ?>
							<button type="button" class="btn btn-link" onclick="save_why_eod_left2()" id="save_why_eod_left2">Save</button>
					 	</div>
					 	<div class="icon"><i class="ion-checkmark"></i>
						<?php }
							if($row_why["title"]=="why_eod_left3"){
						?>
							<span  id="why_eod_left3" contenteditable="true">
									<?php echo $row_why["content"]; ?>
							</span>
									<?php ?>
							<button type="button" class="btn btn-link" onclick="save_why_eod_left3()" id="save_why_eod_left3">Save</button>
					 	</div>
					 	<div class="icon"><i class="ion-checkmark"></i>
						<?php }
							if($row_why["title"]=="why_eod_left4"){
						?>
							<span  id="why_eod_left4" contenteditable="true">
									<?php echo $row_why["content"]; ?>
							</span>
									<?php }} ?>
							<button type="button" class="btn btn-link" onclick="save_why_eod_left4()" id="save_why_eod_left4">Save</button>
					 	</div>			


					</div>

				</div>

			</div>
		</div>
	</div>

</div>

<script>
	document.getElementById('save_why_eod_right').style.visibility="hidden";
	document.getElementById('save_why_eod_left1').style.visibility="hidden";
	document.getElementById('save_why_eod_left2').style.visibility="hidden";
	document.getElementById('save_why_eod_left3').style.visibility="hidden";
	document.getElementById('save_why_eod_left4').style.visibility="hidden";

	document.getElementById("why_eod_right").addEventListener("input", function() {
   	document.getElementById('save_why_eod_right').style.visibility="visible";
}, false);

	document.getElementById("why_eod_left1").addEventListener("input", function() {
   	document.getElementById('save_why_eod_left1').style.visibility="visible";
}, false);

	document.getElementById("why_eod_left2").addEventListener("input", function() {
   	document.getElementById('save_why_eod_left2').style.visibility="visible";
}, false);

	document.getElementById("why_eod_left3").addEventListener("input", function() {
   	document.getElementById('save_why_eod_left3').style.visibility="visible";
}, false);

	document.getElementById("why_eod_left4").addEventListener("input", function() {
   	document.getElementById('save_why_eod_left4').style.visibility="visible";
}, false);

	function save_why_eod_right(){
		var a = document.getElementById('why_eod_right').innerHTML;
		window.location.href = '../php/save_why_eod_right.php?why_eod_right=' + a ;
	}
	function save_why_eod_left1(){
		var a = document.getElementById('why_eod_left1').innerHTML;
		window.location.href = '../php/save_why_eod_left1.php?why_eod_left1=' + a ;
	}
	function save_why_eod_left2(){
		var a = document.getElementById('why_eod_left2').innerHTML;
		window.location.href = '../php/save_why_eod_left2.php?why_eod_left2=' + a ;
	}
	function save_why_eod_left3(){
		var a = document.getElementById('why_eod_left3').innerHTML;
		window.location.href = '../php/save_why_eod_left3.php?why_eod_left3=' + a ;
	}
	function save_why_eod_left4(){
		var a = document.getElementById('why_eod_left4').innerHTML;
		window.location.href = '../php/save_why_eod_left4.php?why_eod_left4=' + a ;
	}
</script>