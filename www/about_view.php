<?php
  
    $sql_about = "SELECT * from home_text"; 
    $result_about = mysqli_query($conn, $sql_about);

?>
<div class="container-fluid" style="background-color: #fee84d;">
<div class="container"   >
	<div class="about">
		<div class="row">
			<div class="col-md-4">
				<div class="vision">
					
					 <div class="icon"><i class="ion-ios-eye"></i></div>
				</div>

					<div class="about-title">
						VISION
					</div>

				<div class="about-text" >
						<?php
							while($row_about = mysqli_fetch_array($result_about)) {
						?>
						<?php  
							if($row_about["title"]=="vision"){
						?>
							<p  id="vision" contenteditable="true">
									
									<?php echo $row_about["content"]; ?>
							</p>
					
				<button type="button" class="btn btn-link" onclick="save_vision()" id="save_vision">Save</button>
				</div>
				
			</div>
			<div class="col-md-4" >
				<div class="mission">
					<div class="icon"><i class="ion-disc"></i></div>
				</div>

					<div class="about-title">
						MISSION
					</div>

				<div class="about-text">
						<?php 
						} 
						if($row_about["title"]=="mission"){ 	?>
							<p  id="mission" contenteditable="true">
								<?php echo $row_about["content"]; ?>
							</p>
							<?php
								
						 	
							?>
							<button type="button" class="btn btn-link" onclick="save_mission()" id="save_mission">Save</button>
				</div>
			</div>
			<div class="col-md-4">
				<div class="goals">
					<div class="icon"><i class="ion-ios-checkmark-outline"></i></div>
				</div>
					<div class="about-title">
						GOALS
					</div>
				<div class="about-text">
						<?php 
						} 
						if($row_about["title"]=="goals"){ 	?>
							<p  id="goals" contenteditable="true">
								<?php echo $row_about["content"]; ?>
							</p>
							<?php
								} 
						 }
							?>
							<button type="button" class="btn btn-link" onclick="save_goals()" id="save_goals">Save</button>
					
				</div>

					
				</div>
				
			</div>

		</div>
	</div>
</div>
</div>


<script>
	
	document.getElementById('save_vision').style.visibility="hidden";
	document.getElementById('save_goals').style.visibility="hidden";
	document.getElementById('save_mission').style.visibility="hidden";
	document.getElementById("vision").addEventListener("input", function() {
   	document.getElementById('save_vision').style.visibility="visible";
}, false);

	document.getElementById("mission").addEventListener("input", function() {
   	document.getElementById('save_mission').style.visibility="visible";
}, false);

	document.getElementById("goals").addEventListener("input", function() {
   	document.getElementById('save_goals').style.visibility="visible";
}, false);


	function save_vision(){
		var a = document.getElementById('vision').innerHTML;
		window.location.href = '../php/save_vision.php?vision=' + a ;
	}
	function save_mission(){
		var a = document.getElementById('mission').innerHTML;
		window.location.href = '../php/save_mission.php?mission=' + a ;
	}
	function save_goals(){
		var a = document.getElementById('goals').innerHTML;
		window.location.href = '../php/save_goals.php?goals=' + a ;
	}


</script>