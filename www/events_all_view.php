
	<?php
    $sqla = "SELECT id,content,title from announcement ORDER BY id DESC"; 
    $resulta = mysqli_query($conn, $sqla);
    $sql_title = "SELECT * from home_text"; 
    $result_title = mysqli_query($conn, $sql_title);

	?>

<div style="background-color: #cbcbcb">
<div class="announcements">
	<div class="container">
		<div class="announcements-title">
		EOD EVENTS
		</div>

		<div class="announcements-sub">
			<?php
							while($row_title = mysqli_fetch_array($result_title)) {
						?>
						<?php  
							if($row_title["title"]=="announcement_sub"){
						?>
							<p  id="announcement_sub" contenteditable="true">
									
									<?php echo $row_title["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_announcement_sub()" id="save_announcement_sub">Save</button>
					</div>
		</div>
		<div class="announcements-content">
		<div class="row">
			<?php
						while($rowa = mysqli_fetch_array($resulta)) {
					?>
			<div class="col-md-6">
				<div class="card">
						   <img src="../php/website_php/announcement-view.php?image_id=<?php echo $rowa["id"]; ?>"  />
				 			
						  <div class="card-footer">
						  		<div class="card-footer-title">
						  	<?php echo $rowa['title'];?>
						  		
						  		</div>
						  		<div class="place">

						  		<!-- 	
								    	<button type="button" id="one-announcement"  >CUBAO</button> -->
								 
						  		</div>
						  		<div class="card-footer-text">
						  			<?php echo $rowa['content'];?>
						  		</div>
						  		<div class="form-row text-center">
								    <div class="col-12">
								    	<button type="button" id="announcements-readmore" class="btn ">READ MORE</button>
								    </div>
								</div>
						  		
						  </div>
						</div>
			</div>
						<?php		
			}
	
		?>
	
		</div>
		</div>
		<br>
	</div>
</div>
</div>

<script>
	document.getElementById('save_announcement_sub').style.visibility="hidden";

	document.getElementById("announcement_sub").addEventListener("input", function() {
   	document.getElementById('save_announcement_sub').style.visibility="visible";
}, false);

	function save_announcement_sub(){
		var a = document.getElementById('announcement_sub').innerHTML;
		window.location.href = '../php/save_announcement_sub_page.php?announcement_sub=' + a ;
	}
</script>