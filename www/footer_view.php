<?php
    $sql_footer = "SELECT * from home_text"; 
    $result_footer = mysqli_query($conn, $sql_footer);

?>


<div class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" >
				<div class="footer_logo">
					<!-- <img src="../imgs/logo_header.png" class="imgs"> -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-title">
					<p>
					eod tactical 
					solutions inc.
					</p>
				</div>
				<div class="footer-text">
						<?php
							while($row_footer= mysqli_fetch_array($result_footer)) {
						?>
						<?php  
							if($row_footer["title"]=="footer_text"){
						?>
							<p  id="footer_text" contenteditable="true">
									
									<?php echo $row_footer["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_footer_text()" id="save_footer_text">Save</button>


								</div>
			</div>
			<div class="col-md-3">
				<div class="footer-title">
					<br>
					<p>
					useful links</p>
				</div>
				<div class="footer-text">
					<div class="icon"><i class="ion-play" ></i>
					 		<span>HOME</span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span>COURSES</span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span>EVENTS</span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span>ABOUT US</span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span>SEND US MESSAGES</span>
					</div>
				</div>

			</div>
			<div class="col-md-3">
				<div class="footer-title"><br>
					<p>
					Contact Us</p>
					</div>
				<div class="footer-text">

				<div>	
					<a href="">
						<i class="fa fa-home" aria-hidden="true"></i>
						<span>Tarlac City</span>
					</a> 
				</div>
				<div>	
					<a href="">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<span>0928 443 8888</span>
					</a>
				</div>
				<div>
					<a href="">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<span>eodtacsolutions@gmail.com</span>
					</a>
				</div>
				<div>
					<a href="">
						<i class="fa fa-facebook" aria-hidden="true"></i>
						<span>facecook.com/Eodtacticalsolutions</span>
					</a>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-end">
	Copyright © 2018 EoD Tactical Solutions Inc. All rights reserved
</div>

<script>
	document.getElementById('save_footer_text').style.visibility="hidden";

	document.getElementById("footer_text").addEventListener("input", function() {
   	document.getElementById('save_footer_text').style.visibility="visible";
}, false);

	function save_footer_text(){
		var a = document.getElementById('footer_text').innerHTML;
		window.location.href = '../php/footer_text.php?footer_text=' + a ;
	}
</script>
