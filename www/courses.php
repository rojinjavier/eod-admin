	<?php
    require_once "../php/connection.php";
    $sql = "SELECT id,course_name,course_description from courses ORDER BY id ASC"; 
    $result = mysqli_query($conn, $sql);
	?>	

	<?php require_once('nav.php'); ?>


	<div class="bg">
	<form name="frmImage" enctype="multipart/form-data" action="../php/course_upload.php"
       		 method="post">
	<div class="announcements">
		<div class="title">
			ADD COURSES
		</div>
		<BR>
		<div class="content">
			<div class="row">

				<div class="col-md-7" style="padding: 2%;margin-left: 30px;">
						<label for="fname">COURSE NAME</label>
			   			 <input type="text" id="title" name="title" placeholder="">
			   			 <label for="fname">COURSE DESCRIPTION</label>
			   			 <textarea placeholder="" class="message" id="content" name="content"></textarea>
				</div>
				<div class="col-md-4">
						
       					 <label>Upload Image File:</label><br /> <input name="userImage"
            type="file" class="inputFile" /> 
 						<button type="submit" value="Submit" class="btn btn-dark" id="choose-images">SAVE IMAGES</button>
						
				</div>
			</div>
			
		</div>
	</div>
	</form>

</div>


<div class="bg">
	<div class="announcements">
		<div class="title">
			CURRENT POSTED COURSES
		</div>
		<div class="row" style="padding: 2%;">
		<?php
		while($row = mysqli_fetch_array($result)) {
		?>
			<div class="col-md-4">
				<div class="card">
						  <img src="../php/course-view.php?image_id=<?php echo $row["id"]; ?>" />
				 			
						  <div class="card-footer">
						  		<div class="card-footer-title">
						  			<?php echo $row['course_name'];?>
						  		
						  		</div>

						  		<div class="card-footer-text">
								<?php echo $row['course_description'];?>
						  		</div>
						  		<div class="form-row text-center">
								    <div class="col-12">
								    	<button type="button" id="announcements-readmore" onclick="remove(<?php echo $row["id"]; ?>)" class="btn ">REMOVE</button>
								    </div>
								</div>
						  		
						  </div>
						</div><br>
			</div>
		<?php		
			}
		    mysqli_close($conn);
		?>
			 
		</div>
	</div>
</div>



 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
  
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you really want to remove/delete this post?</p>
          <p id="postId" hidden=""></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deletepost()">Yes</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	function remove(a){
$('#myModal').modal('show') ;
document.getElementById('postId').innerHTML = a;
	}


	function deletepost(){
		var postId = document.getElementById('postId').innerHTML;
		   var title = "";
    var image ="";
    $.ajax({
        url:'../php/delete_course.php?postId='+postId,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){
     
            }
    })    
	 location.reload();}
	
</script>

