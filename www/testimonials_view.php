<?php
    $sql_testimonial = "SELECT * from home_text"; 
    $result_testimonial = mysqli_query($conn, $sql_testimonial);

?>

<div class="testimonials" style="background-color: #cbcbcb">
	<div class="container">
	<div class="testimonials-title">
		STUDENT TESTIMONIALS
	</div>
	<div class="testimonials-sub">
						<?php
							while($row_testimonial = mysqli_fetch_array($result_testimonial)) {
						?>
						<?php  
							if($row_testimonial["title"]=="testimonial_sub"){
						?>
							<p  id="testimonial_sub" contenteditable="true">
									
									<?php echo $row_testimonial["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_testimonials_sub()" id="save_testimonials_sub">Save</button>
	</div>


<div class="container" style="margin-left: 10%" >
    <div id="carouselContent" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">


            <div class="carousel-item active text-center p-4">


	<div class="testimonials-content">
		<div class="qoute" style="text-align: left;">
			"
		</div>

		<div class="testimonials-text">
			Maecenas tristique felis at luctus volutpat. In ut neque sit amet erat tincidunt pharetra sit amet sit amet est. Mauris gravida aliquam erat ut sodales.
		</div>
		
		<div class="qoute" style="text-align:right">
			"
		</div>
	</div>


            </div>
            <div class="carousel-item text-center p-4">
                
	<div class="testimonials-content">
		<div class="qoute" style="text-align: left;">
			"
		</div>

		<div class="testimonials-text">
			Maecenas tristique felis at luctus volutpat. In ut neque sit amet erat tincidunt pharetra sit amet sit amet est. Mauris gravida aliquam erat ut sodales.
		</div>
		
		<div class="qoute" style="text-align:right">
			"
		</div>
	</div>


            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselContent" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
</div>
</div>
<script>
	document.getElementById('save_testimonials_sub').style.visibility="hidden";

	document.getElementById("testimonial_sub").addEventListener("input", function() {
   	document.getElementById('save_testimonials_sub').style.visibility="visible";
}, false);

	function save_testimonials_sub(){
		var a = document.getElementById('testimonial_sub').innerHTML;
		window.location.href = '../php/save_testimonial_sub.php?testimonial_sub=' + a ;
	}
</script>



