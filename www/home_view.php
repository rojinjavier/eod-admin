<?php
    require_once "../php/website_php/connection.php";
    $sql = "SELECT MAX(id) as a FROM carousel ORDER BY id DESC"; 
    $result = mysqli_query($conn, $sql);
?>
<?php require_once('nav_view.php');?>


<div >

	<div id="demo" class="carousel slide" data-ride="carousel">

	  <!-- Indicators -->
	  <ul class="carousel-indicators">
	  </ul>

	  <!-- The slideshow -->
	  <div class="carousel-inner">
	    <div class="carousel-inner">

	  	
		<?php
		while($row = mysqli_fetch_array($result)) {
		?>
			  <div class="carousel-item active">
			      <img src="../php/website_php/carousel-view.php?image_id=<?php echo $row["a"]; ?>" style="width:100%;height:525px;" />
			    </div>

		<?php		
			}
		    require_once "../php/website_php/connection.php";
		    $sql2 = "SELECT id FROM carousel ORDER BY id DESC LIMIT 1,2"; 
		    $results = mysqli_query($conn, $sql2);
			?>
		<?php
			while($rows = mysqli_fetch_array($results)) {
		?>

				<div class="carousel-item" >
						<img src="../php/website_php/carousel-view.php?image_id=<?php echo $rows["id"]; ?>" style="width:100%;height:525px;" /><br/>
				</div>

		<?php		
			}
	
		?>
			    

			  </div>

	  <!-- Left and right controls -->
	  <a class="carousel-control-prev" href="#demo" data-slide="prev">
	    <span class="carousel-control-prev-icon"></span>
	  </a>
	  <a class="carousel-control-next" href="#demo" data-slide="next">
	    <span class="carousel-control-next-icon"></span>
	  </a>

	</div>
</div>
<?php require_once('about_view.php');?>
<?php require_once('whyEOD_view.php');?>
<?php require_once('courses_view.php');?>
<?php require_once('statistics_view.php');?>
<?php require_once('announcements_view.php');?>
<?php require_once('awards_view.php');?>
<?php require_once('testimonials_view.php');?>
<?php require_once('footer_view.php');?>