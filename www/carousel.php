	<?php
    require_once "../php/connection.php";
    $sql = "SELECT MAX(id) as a FROM carousel ORDER BY id DESC"; 
    $result = mysqli_query($conn, $sql);
	?>
	<?php require_once('nav.php'); ?>


<div class="bg" >
 	<div class="carousel">
 		<div class="title">
 			ADD IMAGE
 		</div>
 		<br>
 		<form name="frmImage" enctype="multipart/form-data" action="../php/carousel-upload.php"
       		 method="post" >
 			<div class="upload-image">

      <img alt="Image Display Here" id="test" src="#" style="height: 400px;width:1000px; padding :  5% 5% 5%  5% " onerror="this.src='../imgs/carousel_blank.png'" />



 				<div class="upload-button">
 					<div class="row">
 						<div class="col-md-4">
 						</div>
  						<div class="col-md-4">
 						</div>
 						<div class="col-md-4" >
 							<div class="fileUpload btn btn-dark" style="width: 300px;color: yellow">
							   <label >Upload</label> <input name="userImage"
           onchange="readURL(this);" type="file" class="inputFile" /> 

							</div>
 						 

 								<button type="submit" style="float: left;width: 300px" value="Submit" class="btn btn-dark" id="choose-images">SAVE IMAGES</button>
 							
 						</div>
 					</div>
 
 				</div>
 			</div>
 		</form>
	</div>
</div>


<div class="bg" >




 	<div class="carousel">
 		<div class="title">
 			PREVIEW
 		</div>
 		<br>
 			<div class="upload-image">
 					<div style="padding: 3%;">

	<div id="demo" class="carousel slide" data-ride="carousel">

	  <!-- Indicators -->
	  <ul class="carousel-indicators">
	  </ul>

	  <!-- The slideshow -->
	  <div class="carousel-inner">

	  	
		<?php
		while($row = mysqli_fetch_array($result)) {
		?>
			  <div class="carousel-item active">
			      <img src="../php/carousel-view.php?image_id=<?php echo $row["a"]; ?>" style="width:100%;height:525px;" />
			    </div>

		<?php		
			}
		    require_once "../php/connection.php";
		    $sql2 = "SELECT id FROM carousel ORDER BY id DESC LIMIT 1,2"; 
		    $results = mysqli_query($conn, $sql2);
			?>
		<?php
			while($rows = mysqli_fetch_array($results)) {
		?>

				<div class="carousel-item" >
						<img src="../php/carousel-view.php?image_id=<?php echo $rows["id"]; ?>" style="width:100%;height:525px;" /><br/>
				</div>

		<?php		
			}
		  
		?>
			    

			  </div>

			  <!-- Left and right controls -->
			  <a class="carousel-control-prev" href="#demo" data-slide="prev">
			    <span class="carousel-control-prev-icon"></span>
			  </a>
			  <a class="carousel-control-next" href="#demo" data-slide="next">
			    <span class="carousel-control-next-icon"></span>
			  </a>

			</div>
		</div>


		 			</div>


<div class="carousel-list">
		<div class="title">
 			List of Images
 		</div>
 		<br>
 			<div class="upload-image">
 					<div style="padding: 3%;">
 				<?php
 				 $sql3 = "SELECT id FROM carousel"; 
		   		 $results3 = mysqli_query($conn, $sql3);
				?>
		<?php
			while($rows3 = mysqli_fetch_array($results3)) {
		?>
				
		<div class="card" style="width:1000px">
    <img class="card-img-top" src="../php/carousel-view.php?image_id=<?php echo $rows3["id"]; ?>" alt="Card image" style="width:100%">
    <div class="card-body">
     
      <button type="button" onclick="remove(<?php echo $rows3['id']?>)" class="btn btn-md btn-danger" style="width: 100%;color:white">Delete this Photo</button>
    </div>
  </div>
<?php		
			}
		  
		?>
				</div>
		 		</div>
				</div>
				</div>
			</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
  
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you really want to remove/delete this Image?</p>
          <p id="imageId" hidden=""></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deletepost()">Yes</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function remove(a){
$('#myModal').modal('show') ;
document.getElementById('imageId').innerHTML = a;
	}

function deletepost(){
		var imageId = document.getElementById('imageId').innerHTML;

    $.ajax({
        url:'../php/delete_image.php?imageId='+imageId,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){
     
            }
    })    
	 location.reload();}
	
</script>