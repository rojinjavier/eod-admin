<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

	<?php require_once('head.php'); ?>

<body id="bg">
	
	<?php
	switch (base64_decode($_GET['admin'])) {
    case 0 : 

      if(isset($_SESSION['login_user'])){
            require_once("../www/home.php");
      }else{

      require_once("../www/login.php");
    }
      break;
    case 1 : 
      if(isset($_SESSION['login_user'])){
            require_once("../www/home.php");
      }else{
        
      require_once("../www/login.php");
    }
      break;

    case 2 : 
      if(isset($_SESSION['login_user'])){
          require_once("../www/announcements.php");
      }else{
          require_once("../www/login.php");
    }
      break;
    case 3 : 
      if(isset($_SESSION['login_user'])){
           require_once("../www/carousel.php");
      }else{
           require_once("../www/login.php");
    }
      break;
    case 4 : 
      if(isset($_SESSION['login_user'])){
          require_once("../www/courses.php");
      }else{  
          require_once("../www/login.php");
    }
      break;
    case 5 :
      require_once("../www/create_account.php");
      break;
    case 6 :
          if(isset($_SESSION['login_user'])){
             require_once("../www/course_pages_view.php");
          }else{  
          require_once("../www/login.php");
          }
      break;
  
      case 7 :
          if(isset($_SESSION['login_user'])){
               require_once("../www/events_pages_view.php");
               }else{  
          require_once("../www/login.php");
              }
      break;
  
      case 8 :
          if(isset($_SESSION['login_user'])){
               require_once("../www/about_pages_view.php");
            }else{  
          require_once("../www/login.php");
         }
      break;
          case 10 :
          if(isset($_SESSION['login_user'])){
             require_once("../www/accounts.php");
          }else{  
          require_once("../www/login.php");
          }
      break;
  }

	?>

	<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script>
  <script src="../lib/superfish/hoverIntent.js"></script>
  <script src="../lib/superfish/superfish.min.js"></script>
  <script src="../lib/wow/wow.min.js"></script>
  <script src="../lib/waypoints/waypoints.min.js"></script>
  <script src="../lib/counterup/counterup.min.js"></script>
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../lib/isotope/isotope.pkgd.min.js"></script>
  <script src="../lib/lightbox/js/lightbox.min.js"></script>
  <script src="../lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="../contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->

</body>
</html>