	<?php
    require_once "../php/connection.php";
    $sql = "SELECT * from officials ORDER BY id ASC"; 
    $result = mysqli_query($conn, $sql);


    $sql2 = "SELECT * from home_text"; 
    $result2 = mysqli_query($conn, $sql2);

	?>	



<div class="about-officials">
	<div class="title">
		EOD OFFICIALS
	</div>
	<div class="sub">
			<?php
							while($row_title2 = mysqli_fetch_array($result2)) {
						?>
						<?php  
							if($row_title2["title"]=="about_officials"){
						?>
							<p  id="about_officials" contenteditable="true">
									
									<?php echo $row_title2["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_about_officials()" id="save_about_officials">Save</button>
	</div>
	<div class="content">
		<div class="container">

			<div class="row">
				<?php
		while($row = mysqli_fetch_array($result)) {
		?>


				<div class="col-md-4">
						<div class="card" style="width: 18rem;">
							
						  <div class="card-body">
						  	 <img src="../php/official_view.php?image_id=<?php echo $row["id"]; ?>" class="imgs"/>
						    <p class="official-name" style=" text-transform: uppercase;"><?php echo $row['official_name'];?> <br><span class="official-position"  style=" text-transform: capitalize;"><?php echo $row['title'];?></span></p>
						    <p class="official-description"><?php echo $row['short_description'];?> </p>

		
						  </div>

						</div>
				<button type="button" onclick="remove(<?php echo $row["id"]; ?>)" class="btn btn-danger btn-block" style="width: 93%;margin-top: 5px">Delete</button>
				</div>


			<?php		
			}
		 
		?>
			 
				<!-- <div class="col-md-4">
						<div class="card" style="width: 18rem;" >
	
						 <div class="card-body">
						  	<img src="../imgs/icon.png" class="imgs">
						    <p class="official-name">	MARK SANTOS <br><span class="official-position">Officer 2</span></p>
						    <p class="official-description">Nullam in rhoncus lectus, interdum mattis sem. Quisque vitae erat eleifend Nullam in rhoncus lectus, interdum mattis sem. Quisque vitae erat eleifend </p>
			
						  </div>
						</div>
				</div>

				<div class="col-md-4">
						<div class="card" style="width: 18rem;">
	
						   <div class="card-body">
						  	<img src="../imgs/icon.png" class="imgs">
						    <p class="official-name">IAN DELA VEGA <br><span class="official-position">Officer 3</span></p>
						    <p class="official-description">Nullam in rhoncus lectus, interdum mattis sem. Quisque vitae erat eleifend Nullam in rhoncus lectus, interdum mattis sem. Quisque vitae erat eleifend </p>
						 
						  </div>
						</div>
				</div> -->
			</div>
		</div>
	</div>
</div>

<div class="add-official">

	<div class="bg">
	<form name="frmImage" enctype="multipart/form-data" action="../php/official_upload.php"
       		 method="post">
	<div class="announcements">
		<div class="title">
			ADD OFFICIAL
		</div>
		<BR>
		<div class="content">
			<div class="row">

				<div class="col-md-7" style="padding: 2%;margin-left: 30px;">
						<label for="fname">OFFICIAL NAME</label>
			   			 <input type="text" id="name" name="name" placeholder="">
			   			 <label for="fname">TITLE</label>
			   			 <input type="text" id="title" name="title" placeholder="">
			   			 <label for="fname">SHORT DESCRIPTION</label>
			   			 <textarea placeholder="" class="message" id="content" name="content"></textarea>
				</div>
				<div class="col-md-4"><br><br>						
       					 <label>Upload Image File:</label><br /> <input name="userImage"
            type="file" class="inputFile" /> 
 						<button type="submit" value="Submit" class="btn btn-dark" id="choose-images">SAVE IMAGES</button>
						
				</div>
			</div>
			
		</div>
	</div>
	</form>

</div>
</div>


 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
  
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you really want to remove/delete this Official?</p>
          <p id="postId" hidden=""></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deletepost()">Yes</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	function remove(a){
$('#myModal').modal('show') ;
document.getElementById('postId').innerHTML = a;
	}


	function deletepost(){
		var postId = document.getElementById('postId').innerHTML;
		   var title = "";
    var image ="";
    $.ajax({
        url:'../php/delete_official.php?id='+postId,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){
     
            }
    })    
	 location.reload();}
	

		document.getElementById('save_about_officials').style.visibility="hidden";

	document.getElementById("about_officials").addEventListener("input", function() {
   	document.getElementById('save_about_officials').style.visibility="visible";
}, false);

	function save_about_officials(){
		var a = document.getElementById('about_officials').innerHTML;
		window.location.href = '../php/save_about_officials.php?text=' + a ;
	}

</script>

