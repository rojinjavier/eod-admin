	<?php

    $sql = "SELECT * from home_text"; 
    $result = mysqli_query($conn, $sql);

	?>


<div class="about-eod"  >
	<div class="container-fluid">
		<div class="row">
		<div class="col-md-6" style="background-color: #fee84d">
			<div class="title-left">
				<p>WHO WE ARE</p>
			</div>
			<div class="content">
					<?php
							while($row_title = mysqli_fetch_array($result)) {
						?>
						<?php  
							if($row_title["title"]=="about-who1"){
						?>
							<p  id="about_who1" contenteditable="true">
									
									<?php echo $row_title["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_about_who1()" id="save_about_who1">Save</button>
					

	<?php

    $sql2 = "SELECT * from home_text"; 
    $result2 = mysqli_query($conn, $sql2);

	while($row_title2 = mysqli_fetch_array($result2)) {
	?>

						<?php  
							if($row_title2["title"]=="about-who2"){
						?>
							<p  id="about_who2" contenteditable="true">
									
									<?php echo $row_title2["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_about_who2()" id="save_about_who2">Save</button>
			</div>


		</div>
		<div class="col-md-6" style="background-color: #ffffff">
			<div class="top-margin">
			<div class="title-right">
				<p>VISION</p>
			</div>
			<div class="content-right">
				
	<?php

    $sql3 = "SELECT * from home_text"; 
    $result3 = mysqli_query($conn, $sql3);

	while($row_title3 = mysqli_fetch_array($result3)) {
	?>

						<?php  
							if($row_title3["title"]=="vision"){
						?>
							<p  id="vision" contenteditable="true">
									
									<?php echo $row_title3["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_vision()" id="save_vision">Save</button>
			</div>
			
			<div class="title-right">
				<p>MISSION</p>
			</div>
			<div class="content-right">
	<?php

    $sql3 = "SELECT * from home_text"; 
    $result3 = mysqli_query($conn, $sql3);

	while($row_title3 = mysqli_fetch_array($result3)) {
	?>

						<?php  
							if($row_title3["title"]=="mission"){
						?>
							<p  id="mission" contenteditable="true">
									
									<?php echo $row_title3["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_mission()" id="save_mission">Save</button>
			</div>
			
			<div class="title-right">
				<p>GOALS</p>
			</div>
			<div class="content-right">
	<?php

    $sql3 = "SELECT * from home_text"; 
    $result3 = mysqli_query($conn, $sql3);

	while($row_title3 = mysqli_fetch_array($result3)) {
	?>

						<?php  
							if($row_title3["title"]=="goals"){
						?>
							<p  id="goals" contenteditable="true">
									
									<?php echo $row_title3["content"]; ?>
							</p><?php  }}?>
				<button type="button" class="btn btn-link" onclick="save_goals()" id="save_goals">Save</button>
			</div>
			
		</div></div>
		</div>
	</div>
	</div>
</div>


<div class="division" style="  padding-left: 28%;">
	"Excelence through World Class Standards"
</div>

<script>
	document.getElementById('save_about_who1').style.visibility="hidden";
	document.getElementById('save_about_who2').style.visibility="hidden";
	document.getElementById('save_vision').style.visibility="hidden";
	document.getElementById('save_mission').style.visibility="hidden";
	document.getElementById('save_goals').style.visibility="hidden";

	document.getElementById("about_who1").addEventListener("input", function() {
   	document.getElementById('save_about_who1').style.visibility="visible";
}, false);

	document.getElementById("about_who2").addEventListener("input", function() {
   	document.getElementById('save_about_who2').style.visibility="visible";
}, false);

	document.getElementById("vision").addEventListener("input", function() {
   	document.getElementById('save_vision').style.visibility="visible";
}, false);

	document.getElementById("mission").addEventListener("input", function() {
   	document.getElementById('save_mission').style.visibility="visible";
}, false);

	document.getElementById("goals").addEventListener("input", function() {
   	document.getElementById('save_goals').style.visibility="visible";
}, false);

	function save_about_who1(){
		var a = document.getElementById('about_who1').innerHTML;
		window.location.href = '../php/save_about_who1.php?who=' + a ;
	}

		function save_about_who2(){
		var a = document.getElementById('about_who2').innerHTML;
		window.location.href = '../php/save_about_who2.php?who=' + a ;
	}
		function save_vision(){
		var a = document.getElementById('vision').innerHTML;
		window.location.href = '../php/save_about_vision.php?vision=' + a ;
	}
		function save_mission(){
		var a = document.getElementById('mission').innerHTML;
		window.location.href = '../php/save_about_mission.php?mission=' + a ;
	}
		function save_goals(){
		var a = document.getElementById('goals').innerHTML;
		window.location.href = '../php/save_about_goals.php?goals=' + a ;
	}
</script>