

	<?php
	require_once "../php/connection.php";
	require_once('nav.php'); 
	$sql = "SELECT * FROM accounts ORDER BY id DESC"; 
    $result = mysqli_query($conn, $sql);
    ?>

	<div style="margin-left:20%" class="accounts">
		<div class="title">
 			List of Accounts
 		</div>
 		<br>
 		<div class="listaccounts">

				
				  <table class="table table-hover">
				    <thead>
				      <tr>
				        <th>Firstname</th>
				        <th>Lastname</th>
				        <th>Username</th>
				        <th colspan="2" style="text-align: center">Actions</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php
						while($row = mysqli_fetch_array($result)) {
						?>
				      <tr>

				        <td><?php echo $row['firstname']?></td>
				        <td><?php echo $row['lastname']?></td>
				        <td><?php echo $row['username']?></td>
				        <td><button type="button" style="width: 120%;" onclick="edit(<?php echo $row['id']?>)" class="btn btn-success">Edit</button></td>
				        <td><button type="button" onclick="remove(<?php echo $row['id']?>)" style="width: 90%;float: right" class="btn btn-danger">Delete</button></td>

				      </tr>
				      <?php		
						}
		  
						?>
			
				    </tbody>
				  </table>


			    
	</div>
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
  
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you really want to remove/delete this Account permanently?</p>
          <p id="imageId" hidden=""></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deletepost()">Yes</button>
           <button type="button" class="btn btn-danger"  data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
  
          <h4 class="modal-title">Edit Accounts</h4>
        </div>
        <div class="modal-body">
          	<div>
  <form>
    <label for="fname">First Name</label>
    <p id="ids" hidden=""></p>
    <input type="text" id="fname" name="firstname" placeholder="Your name..">
    <br>
    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your last name..">
    <br>
        <label for="lname">Last Name</label>
    <input type="text" id="user" name="lastname" placeholder="Your last name..">
    <br>
   
  
       <button type="button" class="btn btn-success" onclick="save()" data-dismiss="modal"  style="width: 65%;" >Save</button>
  </form>
</div>
        </div>
        <div class="modal-footer">
       
           <button type="button" class="btn btn-danger"  data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	function remove(a){
$('#myModal').modal('show') ;
document.getElementById('imageId').innerHTML = a;
	}
function deletepost(){
		var imageId = document.getElementById('imageId').innerHTML;

    $.ajax({
        url:'../php/delete_account.php?imageId='+imageId,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){
     
            }
    })    
	 location.reload();}
	
	function edit(b){
$('#myModal2').modal('show') ;
	var fnames = "";
	var lname ="";
	var username = "";
    $.ajax({
        url:'../php/select_accounts.php?imageId='+b,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){

     	fnames = data[0]['firstname'];
     	lname = data[0]['lastname'];
     	username = data[0]['username'];
     	document.getElementById('ids').innerHTML = data[0]['id'];
     	document.getElementById('fname').value = fnames;
     	document.getElementById('lname').value = lname;
     	document.getElementById('user').value = username;
            }
          
    })    
	}

	function save(){
		var a = document.getElementById('fname').value;
		var b = document.getElementById('lname').value;
		var c = document.getElementById('user').value;
		var id = document.getElementById('ids').innerHTML;
		
	$.ajax({
        url:'../php/edit_account_save.php?fname='+a+'&lname='+b+'&user='+c+'&id='+id,
        type:"POST",
        dataType: "json",
        data:{},
        success: function(data){
     	
            }
          
    })    
	 location.reload();}
</script>